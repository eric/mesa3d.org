---
title:    "Mesa 23.1.6 is released"
date:     2023-08-16
category: releases
tags:     []
---
[Mesa 23.1.6](https://docs.mesa3d.org/relnotes/23.1.6.html) is released.
This is a bug fix release.
