---
title:    "Mesa 23.1.7 is released"
date:     2023-09-06
category: releases
tags:     []
---
[Mesa 23.1.7](https://docs.mesa3d.org/relnotes/23.1.7.html) is released.
This is a bug fix release.
