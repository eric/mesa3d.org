---
title:    "Mesa 22.3.7 is released"
date:     2023-03-08
category: releases
tags:     []
---
[Mesa 22.3.7](https://docs.mesa3d.org/relnotes/22.3.7.html) is released.
This is a bug fix release.
