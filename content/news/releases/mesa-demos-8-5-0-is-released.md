---
title:    "Mesa demos 8.5.0 is released"
date:     2022-05-30
category: releases
tags:     [mesa-demos]
---
Mesa demos 8.5.0 is released. See the
[announcement](https://lists.freedesktop.org/archives/mesa-announce/2022-May/000677.html)
for more information about the release. You can download it from
[archive.mesa3d.org/demos/8.5.0/](https://archive.mesa3d.org/demos/8.5.0/).
