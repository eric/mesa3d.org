---
title:    "Mesa 23.1.4 is released"
date:     2023-07-21
category: releases
tags:     []
---
[Mesa 23.1.4](https://docs.mesa3d.org/relnotes/23.1.4.html) is released.
This is a bug fix release.
