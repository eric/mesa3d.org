---
title:    "Mesa 7.6 is released"
date:     2009-09-28
category: releases
tags:     []
summary:  "[Mesa 7.6](https://docs.mesa3d.org/relnotes/7.6.html) is released. This is a new feature
release. Those especially concerned about stability may want to wait for
the follow-on 7.6.1 bug-fix release."
---
[Mesa 7.6](https://docs.mesa3d.org/relnotes/7.6.html) is released. This is a new feature
release. Those especially concerned about stability may want to wait for
the follow-on 7.6.1 bug-fix release.

[Mesa 7.5.2](https://docs.mesa3d.org/relnotes/7.5.2.html) is also released. This is a stable
release fixing bugs since the 7.5.1 release.
