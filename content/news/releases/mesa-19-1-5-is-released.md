---
title:    "Mesa 19.1.5 is released"
date:     2019-08-23
category: releases
tags:     []
---
[Mesa 19.1.5](https://docs.mesa3d.org/relnotes/19.1.5.html) is released. This is a bug-fix
release.
