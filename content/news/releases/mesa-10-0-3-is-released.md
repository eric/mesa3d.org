---
title:    "Mesa 10.0.3 is released"
date:     2014-02-03
category: releases
tags:     []
---
[Mesa 10.0.3](https://docs.mesa3d.org/relnotes/10.0.3.html) is released. This is a bug-fix
release.
