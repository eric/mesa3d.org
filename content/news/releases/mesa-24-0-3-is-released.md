---
title:    "Mesa 24.0.3 is released"
date:     2024-03-13
category: releases
tags:     []
---
[Mesa 24.0.3](https://docs.mesa3d.org/relnotes/24.0.3.html) is released.
This is a bug fix release.
