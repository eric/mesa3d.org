---
title:    "Mesa 21.0.1 is released"
date:     2021-04-07
category: releases
tags:     []
---
[Mesa 21.0.1](https://docs.mesa3d.org/relnotes/21.0.1.html) is released. This is a bug fix release.
