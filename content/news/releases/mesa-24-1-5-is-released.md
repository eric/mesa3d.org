---
title:    "Mesa 24.1.5 is released"
date:     2024-07-31
category: releases
tags:     []
---
[Mesa 24.1.5](https://docs.mesa3d.org/relnotes/24.1.5.html) is released.
This is a bug fix release.
