---
title:    "Mesa 13.0.0 is released"
date:     2016-11-01
category: releases
tags:     []
---
[Mesa 13.0.0](https://docs.mesa3d.org/relnotes/13.0.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
