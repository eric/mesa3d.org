---
title:    "Mesa 24.1.1 is released"
date:     2024-06-05
category: releases
tags:     []
---
[Mesa 24.1.1](https://docs.mesa3d.org/relnotes/24.1.1.html) is released.
This is a bug fix release.
