---
title:    "Mesa 23.1.1 is released"
date:     2023-05-25
category: releases
tags:     []
---
[Mesa 23.1.1](https://docs.mesa3d.org/relnotes/23.1.1.html) is released.
This is a bug fix release.
