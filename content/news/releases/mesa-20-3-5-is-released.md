---
title:    "Mesa 20.3.5 is released"
date:     2021-03-24
category: releases
tags:     []
---
[Mesa 20.3.5](https://docs.mesa3d.org/relnotes/20.3.5.html) is released. This is a bug fix release.
