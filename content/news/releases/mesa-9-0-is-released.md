---
title:    "Mesa 9.0 is released"
date:     2012-10-08
category: releases
tags:     []
---
[Mesa 9.0](https://docs.mesa3d.org/relnotes/9.0.html) is released. This is the first version of
Mesa to support OpenGL 3.1 and GLSL 1.40 (with the i965 driver). See the
release notes for more information about the release.
