---
title:    "Mesa 22.3.1 is released"
date:     2022-12-14
category: releases
tags:     []
---
[Mesa 22.3.1](https://docs.mesa3d.org/relnotes/22.3.1.html) is released.
This is a bug fix release.
