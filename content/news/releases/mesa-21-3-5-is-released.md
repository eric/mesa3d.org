---
title:    "Mesa 21.3.5 is released"
date:     2022-01-26
category: releases
tags:     []
---
[Mesa 21.3.5](https://docs.mesa3d.org/relnotes/21.3.5.html) is released. This is a bug fix release.
