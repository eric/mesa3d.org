---
title:    "Mesa 17.1.7 is released"
date:     2017-08-21
category: releases
tags:     []
---
[Mesa 17.1.7](https://docs.mesa3d.org/relnotes/17.1.7.html) is released. This is a bug-fix
release.
