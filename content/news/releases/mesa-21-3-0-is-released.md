---
title:    "Mesa 21.3.0 is released"
date:     2021-11-17
category: releases
tags:     []
---
[Mesa 21.3.0](https://docs.mesa3d.org/relnotes/21.3.0.html) is released. This is a new development release. See the release notes for more information about this release.
