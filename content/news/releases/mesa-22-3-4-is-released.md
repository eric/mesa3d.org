---
title:    "Mesa 22.3.4 is released"
date:     2023-01-26
category: releases
tags:     []
---
[Mesa 22.3.4](https://docs.mesa3d.org/relnotes/22.3.4.html) is released.
This is a bug fix release.
