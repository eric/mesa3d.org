---
title:    "Mesa 23.3.6 is released"
date:     2024-02-15
category: releases
tags:     []
---
[Mesa 23.3.6](https://docs.mesa3d.org/relnotes/23.3.6.html) is released.
This is a bug fix release.
