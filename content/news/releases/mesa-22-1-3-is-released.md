---
title:    "Mesa 22.1.3 is released"
date:     2022-06-30
category: releases
tags:     []
---
[Mesa 22.1.3](https://docs.mesa3d.org/relnotes/22.1.3.html) is released. This is a bug fix release.
