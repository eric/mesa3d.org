---
title:    "Mesa 23.1.0 is released"
date:     2023-05-10
category: releases
tags:     []
---
[Mesa 23.1.0](https://docs.mesa3d.org/relnotes/23.1.0.html) is released.
This is a new development release. See the release notes for more information about this release.
