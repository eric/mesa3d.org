---
title:    "Mesa 23.0.0 is released"
date:     2023-02-22
category: releases
tags:     []
---
[Mesa 23.0.0](https://docs.mesa3d.org/relnotes/23.0.0.html) is released.
This is a new development release. See the release notes for more information about this release.
