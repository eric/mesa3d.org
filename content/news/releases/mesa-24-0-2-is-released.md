---
title:    "Mesa 24.0.2 is released"
date:     2024-02-28
category: releases
tags:     []
---
[Mesa 24.0.2](https://docs.mesa3d.org/relnotes/24.0.2.html) is released.
This is a bug fix release.
