---
title:    "Mesa 24.1.0 is released"
date:     2024-05-22
category: releases
tags:     []
---
[Mesa 24.1.0](https://docs.mesa3d.org/relnotes/24.1.0.html) is released.
This is a new development release. See the release notes for more information about this release.
