---
title:    "Mesa 17.3.8 is released"
date:     2018-04-03
category: releases
tags:     []
---
[Mesa 17.3.8](https://docs.mesa3d.org/relnotes/17.3.8.html) is released. This is a bug-fix
release.
