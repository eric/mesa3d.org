---
title:    "Mesa 9.1.1 is released"
date:     2013-03-19
category: releases
tags:     []
---
[Mesa 9.1.1](https://docs.mesa3d.org/relnotes/9.1.1.html) is released. This is a bug fix
release.
