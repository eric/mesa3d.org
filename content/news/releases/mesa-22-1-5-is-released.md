---
title:    "Mesa 22.1.5 is released"
date:     2022-08-03
category: releases
tags:     []
---
[Mesa 22.1.5](https://docs.mesa3d.org/relnotes/22.1.5.html) is released. This is a bug fix release.
